import { NavLink } from 'react-router-dom';

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/">CarCar</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown" aria-expanded="false" aria-label="Toggle navigation" aria-current="true">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item dropdown">
                            <div className="nav-link dropdown-toggle"  id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" aria-current="true">
                                Sales
                            </div>
                            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                <NavLink className="dropdown-item" to="/salespeople/new" aria-current="true">Create a new Sales Person</NavLink>
                                <NavLink className="dropdown-item" to="/customers/new">Create a new Customer</NavLink>
                                <NavLink className="dropdown-item" to="/sales/new">Register a Sale</NavLink>
                                <NavLink className="dropdown-item" to="/salespeople/history">Sales Rep History</NavLink>
                                <NavLink className="dropdown-item" to="/salespeople/list">List of all Sales People</NavLink>
                                <NavLink className="dropdown-item" to="/sales/list">List of all Sales</NavLink>
                                <NavLink className="dropdown-item" to="/customers/list">List of all Customers</NavLink>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <div className="nav-link dropdown-toggle"  id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Service
                            </div>
                            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                <NavLink className="dropdown-item" to="/appointments/new">Create an Appointment</NavLink>
                                <NavLink className="dropdown-item" to="/technicians/new">Hire New Technician</NavLink>
                                <NavLink className="dropdown-item" to="/appointments/list">Appointments</NavLink>
                                <NavLink className="dropdown-item" to="/appointments/history">Service History</NavLink>
                                <NavLink className="dropdown-item" to="/technicians/list">Technicians</NavLink>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <div className="nav-link dropdown-toggle"  id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Inventory
                            </div>
                            <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                <NavLink className="dropdown-item" to="/manufacturers/new">Add a Manufacturer</NavLink>
                                <NavLink className="dropdown-item" to="/automobiles/new">New Automobile</NavLink>
                                <NavLink className="dropdown-item" to="/models/new">New Automobile Model</NavLink>
                                <NavLink className="dropdown-item" to="/automobiles/list">Automobiles</NavLink>
                                <NavLink className="dropdown-item" to="/models/list">Models</NavLink>
                                <NavLink className="dropdown-item" to="/manufacturers/list">Manufacturers</NavLink>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Nav;
