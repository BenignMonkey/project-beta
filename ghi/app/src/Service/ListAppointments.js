import React, { useState, useEffect } from "react";


function AppointmentList () {
    const[appointments, setAppointment] = useState([]);
    const fetchData = async () => {
        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const response = await fetch (appointmentUrl);
        if (response.ok) {
            const data = await response.json()
            setAppointment(data.appointments)
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    const handleFinishAppointment = async (id) => {
        const finishAppointmentUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({"status": "finished"}),
            headers: {
                "Content-Type": "application.json",
            },
        };
        const response = await fetch (finishAppointmentUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    };

    const handleCancelAppointment = async (id) => {
        const cancelAppointmentUrl =`http://localhost:8080/api/appointments/${id}/cancel/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({"status": "cancelled"}),
            headers: {
                "Content-Type": "application.json",
            },
        };
        const response = await fetch(cancelAppointmentUrl, fetchConfig)
        if (response.ok) {
            fetchData();
        }
    };

    return (
        <div className="container my-4">
        <div className="row">
            <div className="col-12">
                <h1 className="text-center mb-4">Service Appointments</h1>
            <table className="table table-dark table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Automobile VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date and Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Finish</th>
                        <th>Cancel</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments?.map(appointment => {
                        if (appointment.status === "created") {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.vip}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{new Date(appointment.date_time).toLocaleString()}</td>
                                    <td>{appointment.technician.first_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button className="btn btn-secondary" onClick={() => handleFinishAppointment(appointment.id)}>Finish</button>
                                    </td>
                                    <td>
                                        <button className="btn btn-secondary" onClick={() => handleCancelAppointment(appointment.id)}>Cancel</button>
                                    </td>
                                </tr>
                            )};
                        })}
                </tbody>
            </table>
            </div>
        </div>
        </div>
    );
}

export default AppointmentList;
