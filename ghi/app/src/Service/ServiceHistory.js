import React, { useState, useEffect } from "react";


function ServiceHistory() {
    const[appointments, setAppointments] = useState([]);
    const[vin, setVin] = useState('');
    const[appointmentsFiltered, setAppointmentsFiltered] = useState([]);

    const fetchData = async () => {
        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const response = await fetch (appointmentUrl);
        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
            setAppointmentsFiltered(data.appointments)
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    const handleSearch = (event) => {
        event.preventDefault();
        if (vin) { setAppointmentsFiltered (
                appointments.filter(appointment =>
                    appointment.vin.toLowerCase().includes(vin.toLowerCase())
                )
            );
        }
    };

    const handleInputChange = (event) => {
        setVin(event.target.value);
    }

    return (
        <div className="container my-4">
        <div className="row">
            <div className="col-12">
                <h1 className="text-center mb-4">Service Appointments</h1>
            <form  onSubmit={handleSearch} id="filter-appointments-jay-wilson-jr">
                <div className="input-group mb-3">
                    <input style={{ backgroundColor: "rgb(228, 230, 240)"}} className="form-control" placeholder="Search for VIN" type="text" name="vin" id="vin" value={vin} onChange={handleInputChange} />
                    <div className="input-group-append">
                        <button className="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            <table className="table table-dark table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Automobile VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date and Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointmentsFiltered.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.vip}</td>
                                <td>{appointment.customer}</td>
                                <td>{new Date(appointment.date_time).toLocaleString()}</td>
                                <td>{appointment.technician.first_name + ' ' + appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            </div>
        </div>
        </div>
    );
}

export default ServiceHistory;
