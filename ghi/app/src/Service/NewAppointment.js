import React, { useState, useEffect } from "react";


function NewAppointmentForm () {
    const [technicians, setTechnician] = useState([]);
    const [customers, setCustomer] = useState([]);
    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date_time: '',
        technician: '',
        reason: '',
    });

    const handleFormChange = (event) => {
        const inputName = event.target.name;
        const value = event.target.value;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application.json",
            },
        };

        const response = await fetch (appointmentUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                vin: '',
                customer: '',
                date_time: '',
                technician: '',
                reason: '',
            });
        }
    }

    const fetchTechnician = async () => {
        const techniciansUrl = "http://localhost:8080/api/technicians/";
        const response = await fetch(techniciansUrl);
        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technicians);
        }
    };
    useEffect(() => {
        fetchTechnician();
    }, []);

    const fetchCustomer = async () => {
        const customersUrl = "http://localhost:8090/api/customers/";
        const response = await fetch(customersUrl);
        if (response.ok) {
            const data = await response.json();
            setCustomer(data.customers);
        }
    };
    useEffect(() => {
        fetchCustomer();
    }, []);

    return (
        <div className="my-5 container">
            <div className="row">
                <form onSubmit={handleSubmit} id="create-AppointmentForm-erin">
                    <h1 className="mb-3">
                        Appointment Scheduling
                    </h1>
                    <div className="row">
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.vin} required placeholder="vin" type="text" id="vin" name="vin" className="form-control"/>
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>
                    </div>
                    <div className="mb-3">
                        <select style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.customer} name="customer" id="customer" className="dropdownClasses" required>
                            <option value="">Customer</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.first_name + ' ' + customer.last_name}>
                                        {customer.first_name + ' ' + customer.last_name}
                                    </option>
                                );
                                })}
                        </select>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.date_time} required placeholder="date_time" type="datetime-local" id="date_time" name="date_time" className="form-control"/>
                            <label htmlFor="date_time">Date and Time</label>
                        </div>
                    </div>
                    <div className="mb-3">
                        <select style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.technician} name="technician" id="technician" className="dropdownClasses" required>
                            <option value="">Technician</option>
                            {technicians.map(technician => {
                                return (
                                    <option key={technician.id} value={technician.id}>
                                        {technician.first_name}
                                    </option>
                                );
                                })}
                        </select>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.reason} required placeholder="reason" type="text" id="reason" name="reason" className="form-control"/>
                            <label htmlFor="reason">Reason for Visit</label>
                        </div>
                    </div>
                    </div>
                    <button className="btn btn-lg btn-secondary">Schedule</button>
                </form>
            </div>
        </div>
    )
}

export default NewAppointmentForm;
