import React from 'react';

const Footer = () => {
    const year = new Date().getFullYear();
    return <footer>{`Copyright © Jay Connolly Benitez Designs ${year}`}</footer>;
};

export default Footer;
