import React, { useState, useEffect } from "react";

function ListCustomers() {
    const [customers, setCustomers] = useState([]);
    const fetchData = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers);
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    const deleteCustomer = async(id) => {
        const url = `http://localhost:8090/api/customers/${id}`;
        const response = await fetch(url, {method: "DELETE"});
        if (response.ok) {
            setCustomers(customers.filter(customer => customer.id !== id));
        }
    };


    return (
        <div className="container my-4">
        <div className="row">
            <div className="col-12">
                <h1 className="text-center mb-4">Customer List</h1>
            <table className="table table-dark table-striped table-bordered">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                {customers?.map((customer) => {
                    return (
                        <tr key={customer.id}>
                            <td>{customer.first_name}</td>
                            <td>{customer.last_name}</td>
                            <td>{customer.address}</td>
                            <td>{customer.phone_number}</td>
                            <td>
                                <button className="btn btn-secondary" onClick={() => deleteCustomer(customer.id)}>Remove</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
        </div>
            {customers.length === 0 && (
        <div className="row">
            <div className="col-12 text-center">
                <p className="lead">Loading...</p>
            </div>
        </div>
    )}
    </div>
    );
}

export default ListCustomers;
