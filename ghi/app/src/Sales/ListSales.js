import React, { useState, useEffect } from "react";

function ListSale() {
    const [sales, setSales] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
            }
    };
    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="container my-4">
        <div className="row">
            <div className="col-12">
                <h1 className="text-center mb-4">Sales</h1>
            <table  className="table table-dark table-striped table-bordered">
            <thead style={{color: "rgb(228, 230, 240)"}}>
                <tr>
                    <th>Salesperson  Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody >
                {sales?.map((sale) => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.sales_person.employee_id}</td>
                            <td>{sale.sales_person.first_name + " " + sale.sales_person.last_name}</td>
                            <td>{sale.customer.first_name + " " + sale.customer.last_name} </td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price_field}</td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
        </div>
    </div>
    );
}

export default ListSale;
