import React, {useState, useEffect } from "react";


function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState(" ");

    const handleSalespersonChange = (event) => {
        const value = parseInt(event.target.value);
        setSalesperson(value);
    }

    const fetchData = async () => {
        const salespeopleURL = "http://localhost:8090/api/salespeople/";
        const salespeopleResponse = await fetch(salespeopleURL);
        if (salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.sales_people);
        }

        const salesURL = "http://localhost:8090/api/sales/"
        const salesResponse = await fetch(salesURL);
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            setSales(salesData.sales);
        }
    }

    const filteredSales = () => {
        const filtered = sales.filter(sale => sale.sales_person.id === salesperson);
        return filtered;
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container my-5">
    <h1 className="mb-3">Salesperson History</h1>
    <div className="row mb-3">
        <div className="col-4">
            <label htmlFor="salesperson-select" className="form-label">Select a Salesperson:</label>
            <select style={{ backgroundColor: "rgb(228, 230, 240)"}} id="salesperson-select" className="form-select" value={salesperson} onChange={handleSalespersonChange}>
                <option value="">Select a Salesperson</option>
                {salespeople.map((sales_person) => (
                    <option key={sales_person.id} value={sales_person.id}>
                        {sales_person.first_name} {sales_person.last_name}
                    </option>
                ))}
            </select>
        </div>
    </div>
    <div className="row">
        <div className="col">
            <table className="table table-dark table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales().map((sale) => {
                        return (
                        <tr key={sale.id} value={sale.id}>
                            <td>{sale.sales_person.first_name} {sale.sales_person.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price_field}</td>
                        </tr>
                        );
                        })}
                </tbody>
            </table>
        </div>
    </div>
</div>
    );
                    }

export default SalespersonHistory;
