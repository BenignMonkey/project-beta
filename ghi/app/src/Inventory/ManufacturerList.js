import React, { useState, useEffect } from "react";


function ManufacturerList () {
    const[manufacturers, setManufacturer] = useState([]);
    const fetchData = async () => {
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch (manufacturerUrl);
        if (response.ok) {
            const data = await response.json()
            setManufacturer(data.manufacturers)
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    const deleteManufacturer= async(id) => {
        const manufacturerUrl = `http://localhost:8100/api/manufacturers/${id}`;
        const response = await fetch(manufacturerUrl, {method: "DELETE"});
        if (response.ok) {
            setManufacturer(manufacturers.filter(manufacturer => manufacturer.id !== id));
        }
    };

    return (
        <div className="container my-4">
        <div className="row">
            <div className="col-12">
                <h1 className="text-center mb-4">Manufacturers</h1>
            <table className="table table-dark table-striped table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers?.map(manufacturer => {
                    return (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                            <td>
                                <button className="btn btn-secondary" onClick={() => deleteManufacturer(manufacturer.id)}>Remove</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
            </div>
        </div>
        </div>
    );
}

export default ManufacturerList;
