import React, { useState } from "react";


function NewManufacturerForm () {
    const[formData, setFormData] = useState({
        name: '',

    });

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application.json'
            },
        };

        const response = await fetch (manufacturerUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
            });
        }
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <form onSubmit={handleSubmit} id="create-Manufacturer">
                    <h1 className="mb-3">
                        New Manufacturer
                    </h1>
                    <div className="row">
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.name} required placeholder="name" type="text" id="name" name="name" className="form-control"/>
                            <label htmlFor="name">Manufacturer Name</label>
                        </div>
                    </div>
                    </div>
                    <button className="btn btn-lg btn-secondary">Create!</button>
                </form>
            </div>
        </div>
    )
}

export default NewManufacturerForm;
