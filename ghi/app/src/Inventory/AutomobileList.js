import React, { useState, useEffect } from "react";


function AutomobileList () {
    const[automobiles, setAutomobiles] = useState([]);
    const[sales, setSales] = useState([]);
    const fetchData = async () => {
        const automobilesUrl = "http://localhost:8100/api/automobiles/";
        const response = await fetch (automobilesUrl);
        if (response.ok) {
            const data = await response.json()
            setAutomobiles(data.autos)
        }
    };
    const fetchSales = async () => {
        const salesUrl = "http://localhost:8090/api/sales/";
        const response = await fetch (salesUrl);
        if (response.ok) {
            const data = await response.json()
            const vins = []
            for (let sale of data.sales){
                vins.push(sale.automobile.vin)
            }
            setSales(vins)
        }
    };
    useEffect(() => {
        fetchData();
        fetchSales();
    }, []);

    const deleteAutomobile = async(vin) => {
        const automobileUrl = `http://localhost:8100/api/automobiles/${vin}`;
        const response = await fetch(automobileUrl, {method: "DELETE"});
        if (response.ok) {
            setAutomobiles(automobiles.filter(automobile => automobile.id !== vin));
        }
    };

    return (
        <div className="container my-4">
        <div className="row">
            <div className="col-12">
                <h1 className="text-center mb-4">Automobiles</h1>
            <table className="table table-dark table-striped table-bordered">
            <thead>
                <tr>
                    <th>Automobile VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                {automobiles?.map(automobile => {
                    return (
                        <tr key={automobile.id}>
                            <td>{automobile.vin}</td>
                            <td>{automobile.color}</td>
                            <td>{automobile.year}</td>
                            <td>{automobile.model.name}</td>
                            <td>{automobile.model.manufacturer.name}</td>
                            <td>{sales.includes(automobile.vin) ? 'yes' : 'no'}</td>
                            <td>
                                <button className="btn btn-secondary" onClick={() => deleteAutomobile(automobile.id)}>Remove</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
            </div>
        </div>
        </div>
    );
}

export default AutomobileList;
